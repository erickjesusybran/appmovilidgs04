/**
 * Función que retorna true/false dependiendo de la estructura del correo electrónico.
 * @param {*} correoElectronico 
 * @returns 
 */
export function emailValidation(correoElectronico = "") {
    const emailValid = /^([a-zA-Z0-9_.])+@(([a-zA-Z0-9])+\.)+([a-zA-Z0-9]{2,4})+$/;

    const resultValidation = emailValid.test(correoElectronico);

    if (resultValidation)
        return true;
    else
        return false;
}

/**
 * Función que retorna true/false dependiendo del tamaño del dato.
 * @param {*} data 
 * @param {*} minLength 
 * @returns 
 */
export function minLengthValidation(data = "", minLength = 0) {
    if (data.length >= minLength)
        return true;
    else
        return false;
}