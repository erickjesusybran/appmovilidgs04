// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyA8DpiQrSelTztpCNLcdypoQuDlpUtIOn4",
    authDomain: "emoprisma-a20df.firebaseapp.com",
    projectId: "emoprisma-a20df",
    storageBucket: "emoprisma-a20df.appspot.com",
    messagingSenderId: "432927942816",
    appId: "1:432927942816:web:86b4f02d2db0c1951401c5"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export { app };