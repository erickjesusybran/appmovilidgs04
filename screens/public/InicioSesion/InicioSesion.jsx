import React, { useState } from 'react'
import { Image } from 'react-native'
import {
	Input, Button, Center,
	useToast, Heading, VStack, Text, Box
} from 'native-base'
import { app } from '../../../firebase/config'
import { getFirestore, collection, getDocs, where, query } from 'firebase/firestore'
import { getAuth, signInWithEmailAndPassword } from "firebase/auth"
import { focusStyles } from '../../../utils/styles/inputs'

export default function InicioSesion({ navigation }) {
	const toast = useToast();
	const db = getFirestore(app);
	const auth = getAuth(app);
	const usuariosRef = collection(db, "usuarios");

	const [email, setEmail] = useState("");
	const [pass, setPass] = useState("");
	const [isLoading, setIsLoading] = useState(false);

	const iniciarSesionAuth = () => {
		//Inicia carga
		setIsLoading(true);

		signInWithEmailAndPassword(auth, email, pass)
			.then((userCredential) => {
				// Signed in
				getUsuarioDoc(userCredential.user);
			})
			.catch((error) => {
				setIsLoading(false);
				toast.show({ description: error.message })
			})
	}

	const getUsuarioDoc = (user) => {
		const q = query(usuariosRef, where("idAuth", "==", user.uid));

		getDocs(q).then(qS => {
			let usuarioDoc = null;

			usuarioDoc = { ...qS.docs[0].data(), id: qS.docs[0].id }

			setIsLoading(false);
			toast.show({ description: 'Bienvenido ' + usuarioDoc.nombres });
			navigation.navigate('MainTab', { user: usuarioDoc });
		})
	}

	return (
		<Center flex={1} px={4} bg={'rose.500'}>
			<VStack mx={2} my={2} p={6} space={5} bg={'white'} borderRadius='2xl'>
				<Center>
					<Image
						source={require('./../../../assets/images/icon.png')}
						style={{
							width: 150,
							height: 150
						}} />
				</Center>
				<Box>
					<Heading textAlign={'center'}>
						Bienvenido a <Text color={'rose.600'}>EmoPrisma</Text>
					</Heading>
				</Box>
				<Text>* Todos los campos son requeridos</Text>
				<VStack space={4}>
					<Input
						defaultValue=""
						placeholder="Correo electrónico"
						keyboardType='email-address'
						onChangeText={(value => setEmail(value))}
						_focus={focusStyles} />
					<Input
						type="password"
						keyboardType='number-pad'
						defaultValue=""
						placeholder="Contraseña"
						onChangeText={(value => setPass(value))}
						_focus={focusStyles} />
				</VStack>
				<Button
					marginTop={8}
					shadow={2}
					colorScheme={'pink'}
					isLoading={isLoading}
					onPress={iniciarSesionAuth}>
					INICIAR SESIÓN
				</Button>
				<Center>
					<Text>¿No tienes una cuenta aún?</Text>
					<Button size="sm" colorScheme={'rose'} variant="link" onPress={() => {
						navigation.navigate('Registro')
					}}>
						Registrarse
					</Button>
				</Center>
			</VStack>
		</Center>
	)
}