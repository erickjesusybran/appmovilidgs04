import React, { useState, useEffect } from 'react'
import { Image } from 'react-native'
import {
	Box, FormControl, Input, Button, useToast, VStack, ScrollView,
	Heading, Text, Select, Center
} from 'native-base'
import { app } from '../../../firebase/config';
import { getAuth, createUserWithEmailAndPassword } from 'firebase/auth'
import { addDoc, collection, getDocs, getFirestore } from 'firebase/firestore'
import { focusStyles } from '../../../utils/styles/inputs';
import { emailValidation, minLengthValidation } from '../../../utils/validation/Form';

/**
 * Componente de React Native que despliega la vista de registro de la app.
 * 
 * @param {*} {navigation}
 * @returns React.Node
 */
export default function Registro({ navigation }) {
	//Instancias de Firebase/NativeBase
	const db = getFirestore(app);
	const auth = getAuth(app);
	const toast = useToast();

	//Refs
	const escuelasRef = collection(db, 'escuelas');

	const [escuelas, setEscuelas] = useState([]);
	const [usuario, setUsuario] = useState({
		nombres: '',
		apellidos: '',
		email: '',
		contrasena: '',
		escuela: '',
		edad: '0',
		fotoPerfil: '',
		materias: []
	});
	const [confirmarContrasena, setConfirmarContrasena] = useState("");
	const [isLoading, setIsLoading] = useState(false);

	useEffect(() => {
		getEscuelasDocs();

		return () => {
			setEscuelas([]);
		}
	}, []);

	/**
	 * Función para traer las escuelas
	 * @returns void
	 */
	const getEscuelasDocs = () => {
		getDocs(escuelasRef).then(querySnapshot => {
			let escuelasStored = [];
			querySnapshot.forEach(doc => {
				escuelasStored.push(doc.data());
			})

			setEscuelas(escuelasStored);
		});
	}

	/**
	 * Función para crear el usuario
	 * @returns void
	 */
	const addUsuarioDoc = () => {
		const usuarioData = usuario;

		if (!minLengthValidation(usuarioData.nombres, 3)) {
			toast.show({ description: "Ingrese sus nombres" });
			return;
		}

		if (!minLengthValidation(usuarioData.apellidos, 3)) {
			toast.show({ description: "Ingrese sus apellidos" });
			return;
		}

		if (!emailValidation(usuarioData.email)) {
			toast.show({ description: "Ingrese una dirección de correo electrónico válida" });
			return;
		}

		if (!minLengthValidation(usuarioData.contrasena, 6)) {
			toast.show({ description: "Ingrese una contraseña de al menos 6 caracteres" });
			return;
		}

		if (!minLengthValidation(usuarioData.nombres, 3)) {
			toast.show({ description: "Ingrese sus nombres" });
			return;
		}

		if (!minLengthValidation(usuarioData.escuela, 1)) {
			toast.show({ description: "Seleccione una escuela" });
			return;
		}

		if (!minLengthValidation(usuarioData.edad, 1)) {
			toast.show({ description: "Ingresa tu edad" });
			return;
		}

		if (usuarioData.contrasena !== confirmarContrasena) {
			toast.show({ description: "Las contraseñas deben coincidir" });
			return;
		}

		//Inicia carga y crea el usuario
		setIsLoading(true);

		createUserWithEmailAndPassword(auth, usuarioData.email, usuarioData.contrasena)
			.then(userAuthCreated => {
				//Crea el usuario en los documentos
				addDoc(collection(db, 'usuarios'), {
					...usuarioData,
					idAuth: userAuthCreated.user.uid
				})
					.then(usuarioDocCreated => {
						setIsLoading(false);
						toast.show({ description: `Bienvenido: ${usuarioData.nombres}` })
						navigation.navigate('MainTab',
							{
								user:
								{
									...usuarioData,
									id: usuarioDocCreated.id
								}
							});
					});
			}).catch(e => {
				setIsLoading(false);
				console.log(e.message);
			});
	}

	return (
		<ScrollView>
			<Box flex={1} px={6} py={4} bg={'white'}>
				<VStack>
					<Center>
						<Image
							source={require('./../../../assets/images/register.png')}
							style={{
								width: 150,
								height: 100
							}} />
					</Center>
					<Heading textAlign={'center'}>Crear una cuenta</Heading>
					<VStack space={4} my={4}>
						<Text color={'darkBlue.800'}>* Todos los campos son requeridos</Text>
						<FormControl>
							<FormControl.Label>Nombre completo</FormControl.Label>
							<Input
								onChange={e => setUsuario({ ...usuario, nombres: e.nativeEvent.text })}
								placeholder="Nombres"
								_focus={focusStyles} />
							<FormControl.ErrorMessage>
								Ingrese un nombre valido
							</FormControl.ErrorMessage>
						</FormControl>
						<Input onChange={e => setUsuario({ ...usuario, apellidos: e.nativeEvent.text })} placeholder="Apellidos" _focus={focusStyles} />
						<Select
							placeholder='Selecciona una escuela'
							onValueChange={i => setUsuario({ ...usuario, escuela: i })}>
							{
								escuelas.map((value, index) => (
									<Select.Item label={value.nombre} value={value.nombre} key={index} />
								))
							}
						</Select>
						<Input
							onChange={e => setUsuario({ ...usuario, email: e.nativeEvent.text })}
							placeholder="Correo electrónico"
							keyboardType="email-address"
							_focus={focusStyles} />
						<Input
							onChange={e => setUsuario({ ...usuario, contrasena: e.nativeEvent.text })}
							placeholder="Contraseña"
							keyboardType="number-pad"
							secureTextEntry
							_focus={focusStyles} />
						<Input onChange={e => setConfirmarContrasena(e.nativeEvent.text)}
							placeholder="Confirmar contraseña"
							keyboardType="number-pad"
							secureTextEntry
							_focus={focusStyles} />
						<Input
							onChange={e => setUsuario({ ...usuario, edad: e.nativeEvent.text })}
							placeholder="Edad"
							keyboardType="number-pad"
							_focus={focusStyles} />
					</VStack>
					<Button
						isLoading={isLoading}
						onPress={addUsuarioDoc}
						marginTop={8}
						colorScheme={'pink'}>
						CREAR CUENTA AHORA
					</Button>
				</VStack>
			</Box >
		</ScrollView >
	)
}