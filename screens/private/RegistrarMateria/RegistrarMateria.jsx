import {
  Box,
  Button,
  Center,
  FormControl,
  Heading,
  Input,
  Radio,
  ScrollView,
  Select,
  Text,
  VStack,
  useToast,
} from "native-base";
import React, { useState, useEffect } from 'react'
import { app } from "../../../firebase/config";
import {
  getFirestore,
  collection,
  getDocs,
  where,
  query,
  updateDoc,
  doc,
} from "firebase/firestore";
import { focusStyles } from "../../../utils/styles/inputs";
import { emailValidation, minLengthValidation } from '../../../utils/validation/Form';

function RegistrarMateria({ route, navigation }) {
  const db = getFirestore(app);

   //obtenemos los datos del usuario del route
   const { usuario } = route.params;

  //Refs
	const periodosRef = collection(db, 'periodos');
  const usuariosRef = doc(db, 'usuarios', usuario.id);

  const toast = useToast();

	//estados
	const [periodos, setPeriodos] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
  const [usuarioUpdate, setUsuarioUdate] = useState(usuario);
  //creamos nuestro estado de la materia que vamos a meter al usuario
  const [materia, setMateria] = useState({
		emocion: 'Felicidad',
		nombre: '',
		periodo: {}
	
	});

  //asignamos el objeto de periodo que eligio el usuario  a la materia
  const asignarPeriodoAMateria = (periodoParametro) =>{
    periodos.map((periodoMap, index)=>{
      if(periodoMap.grado == periodoParametro)
      {
        setMateria({...materia, periodo: periodoMap})
      } 
    })
  }

	/**
	 * Función para traer los periodos
	 * @returns void
	 */
	const getPeriodosDocs = () => {
		getDocs(periodosRef).then(querySnapshot => {
			let periodosStored = [];
			querySnapshot.forEach(doc => {
				periodosStored.push(doc.data());
			})

			setPeriodos(periodosStored);
		});
	}

  const updateUsuario = () =>{

    //validaciones
    if (!minLengthValidation(materia.nombre, 1)) {
			toast.show({ description: "Ingrese un nombre" });
      setIsLoading(false)
			return;
		}

		if (materia.periodo.grado == undefined) {
			toast.show({ description: "Seleccione un periodo" });
      setIsLoading(false)
			return;
		}

    //agregamos la nueva materia a usuario
    let usuario2  = usuario;
    usuario2.materias.push(materia);
    const usuarioData = usuario2;

    //hacemos el cambio en firestore
    updateDoc(usuariosRef,usuarioData)
    .then(()=>{ 
      toast.show({ description: "Materia registrada"})
      navigation.navigate('Mis materias')
    })
    .catch(()=>{ 
      setIsLoading(false)
      toast.show({ description: "Hubo un erro intentalo mas tarde"})
    })
  }

    //traemos los periodos
	useEffect(() => {
		getPeriodosDocs();

		return () => {
			setPeriodos([]);
		}
	}, []);

  return (
    <ScrollView>
      <Box flex={1} px={6} py={4} bg={"white"}>
        <VStack>
          <Heading textAlign={"center"}>Registra una materia</Heading>
          <VStack space={4} my={4}>
            <Text color={"darkBlue.800"}>Elige una emoción</Text>
            <Radio.Group
              defaultValue="Felicidad"
              name="myRadioGroup"
              accessibilityLabel="Pick your favorite number"
              onChange={e => setMateria({...materia, emocion: e})}
            >
              <Radio value="Felicidad" my={1}>
                Felicidad
              </Radio>
              <Radio value="Ira" my={1}>
                Ira
              </Radio>
              <Radio value="Tristeza" my={1}>
                Tristeza
              </Radio>
              <Radio value="Miedo" my={1}>
                Miedo
              </Radio>
              <Radio value="Disgusto" my={1}>
                Disgusto
              </Radio>
              <Radio value="Sorpresa" my={1}>
                Sorpresa
              </Radio>
            </Radio.Group>
            <FormControl>
              <FormControl.Label>Nombre de la materia</FormControl.Label>
              <Input placeholder="Nombre" _focus={focusStyles} 
              onChange={e => setMateria({...materia, nombre: e.nativeEvent.text})}/>
              <FormControl.ErrorMessage>
                Ingrese un nombre valido
              </FormControl.ErrorMessage>
            </FormControl>
            <Select
							placeholder='Selecciona una periodo'
							onValueChange={i => asignarPeriodoAMateria(i)}>
							{
								periodos.map((value, index) => (
									<Select.Item label={value.grado + '°'} value={value.grado} key={index} />
								))
							}
						</Select>
          </VStack>
          <Button isLoading={isLoading} marginTop={8} colorScheme={"pink"}
          onPress={ () =>{
            setIsLoading(true)
            updateUsuario()
          }}>
            GUARDAR MATERIA
          </Button>
        </VStack>
      </Box>
    </ScrollView>
  );
}

export { RegistrarMateria };
