import React, { useEffect, useState } from 'react';
import { Dimensions } from 'react-native';
import {
  getDocs,
  getFirestore,
  query,
  where,
  collection,
} from 'firebase/firestore';

import { Box, Center, Heading, ScrollView, Spinner, Text } from 'native-base';
import { PieChart } from 'react-native-chart-kit';

import { app } from '../../../firebase/config';
import { RefreshControl } from 'react-native';

function Tablero({ navigation }) {
  const db = getFirestore(app);
  let ScreenWidth = Dimensions.get('window').width;
  const [grafica, setGrafica] = useState(null);

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getGrafica();
  }, [isLoading]);

  /**
   * Función para traer y mapear la gráfica
   */
  const getGrafica = () => {
    getDocs(collection(db, 'usuarios')).then((qS) => {
      let resultados = [];
      let graficaCantidades = [
        {
          felicidad: 0,
          ira: 0,
          tristeza: 0,
          miedo: 0,
          disgusto: 0,
          sorpresa: 0,
        },
        {
          felicidad: 0,
          ira: 0,
          tristeza: 0,
          miedo: 0,
          disgusto: 0,
          sorpresa: 0,
        },
        {
          felicidad: 0,
          ira: 0,
          tristeza: 0,
          miedo: 0,
          disgusto: 0,
          sorpresa: 0,
        },
        {
          felicidad: 0,
          ira: 0,
          tristeza: 0,
          miedo: 0,
          disgusto: 0,
          sorpresa: 0,
        },
        {
          felicidad: 0,
          ira: 0,
          tristeza: 0,
          miedo: 0,
          disgusto: 0,
          sorpresa: 0,
        },
        {
          felicidad: 0,
          ira: 0,
          tristeza: 0,
          miedo: 0,
          disgusto: 0,
          sorpresa: 0,
        },
      ];

      qS.forEach((doc) => {
        if (doc.data().materias.length > 0) {
          resultados.push(doc.data().materias);
        }
      });

      //Recorremos el primer arreglo de materias
      resultados.forEach((arrMaterias) => {
        //Recorremos el arreglo de la materia interno
        arrMaterias.forEach((objMaterias) => {
          switch (objMaterias.periodo.grado) {
            case 1:
              switch (objMaterias.emocion) {
                case 'Felicidad':
                  graficaCantidades[0].felicidad += 1;
                  break;
                case 'Ira':
                  graficaCantidades[0].ira += 1;
                  break;
                case 'Tristeza':
                  graficaCantidades[0].tristeza += 1;
                  break;
                case 'Miedo':
                  graficaCantidades[0].miedo += 1;
                  break;
                case 'Disgusto':
                  graficaCantidades[0].disgusto += 1;
                  break;
                case 'Felicidad':
                  graficaCantidades[0].sorpresa += 1;
                  break;

                default:
                  break;
              }
              break;
            case 2:
              switch (objMaterias.emocion) {
                case 'Felicidad':
                  graficaCantidades[1].felicidad += 1;
                  break;
                case 'Ira':
                  graficaCantidades[1].ira += 1;
                  break;
                case 'Tristeza':
                  graficaCantidades[1].tristeza += 1;
                  break;
                case 'Miedo':
                  graficaCantidades[1].miedo += 1;
                  break;
                case 'Disgusto':
                  graficaCantidades[1].disgusto += 1;
                  break;
                case 'Felicidad':
                  graficaCantidades[1].sorpresa += 1;
                  break;

                default:
                  break;
              }
              break;
            case 3:
              switch (objMaterias.emocion) {
                case 'Felicidad':
                  graficaCantidades[0].felicidad += 1;
                  break;
                case 'Ira':
                  graficaCantidades[2].ira += 1;
                  break;
                case 'Tristeza':
                  graficaCantidades[2].tristeza += 1;
                  break;
                case 'Miedo':
                  graficaCantidades[2].miedo += 1;
                  break;
                case 'Disgusto':
                  graficaCantidades[2].disgusto += 1;
                  break;
                case 'Felicidad':
                  graficaCantidades[2].sorpresa += 1;
                  break;

                default:
                  break;
              }
              break;
            case 4:
              switch (objMaterias.emocion) {
                case 'Felicidad':
                  graficaCantidades[3].felicidad += 1;
                  break;
                case 'Ira':
                  graficaCantidades[3].ira += 1;
                  break;
                case 'Tristeza':
                  graficaCantidades[3].tristeza += 1;
                  break;
                case 'Miedo':
                  graficaCantidades[3].miedo += 1;
                  break;
                case 'Disgusto':
                  graficaCantidades[3].disgusto += 1;
                  break;
                case 'Felicidad':
                  graficaCantidades[3].sorpresa += 1;
                  break;

                default:
                  break;
              }
              break;
            case 5:
              switch (objMaterias.emocion) {
                case 'Felicidad':
                  graficaCantidades[4].felicidad += 1;
                  break;
                case 'Ira':
                  graficaCantidades[4].ira += 1;
                  break;
                case 'Tristeza':
                  graficaCantidades[4].tristeza += 1;
                  break;
                case 'Miedo':
                  graficaCantidades[4].miedo += 1;
                  break;
                case 'Disgusto':
                  graficaCantidades[4].disgusto += 1;
                  break;
                case 'Felicidad':
                  graficaCantidades[4].sorpresa += 1;
                  break;

                default:
                  break;
              }
              break;
            case 6:
              switch (objMaterias.emocion) {
                case 'Felicidad':
                  graficaCantidades[5].felicidad += 1;
                  break;
                case 'Ira':
                  graficaCantidades[5].ira += 1;
                  break;
                case 'Tristeza':
                  graficaCantidades[5].tristeza += 1;
                  break;
                case 'Miedo':
                  graficaCantidades[5].miedo += 1;
                  break;
                case 'Disgusto':
                  graficaCantidades[5].disgusto += 1;
                  break;
                case 'Felicidad':
                  graficaCantidades[5].sorpresa += 1;
                  break;

                default:
                  break;
              }
              break;

            default:
              break;
          }
        });
      });

      //Declaramos nuestro modelo de la grafica 1
      let grafica1Model = [
        {
          name: 'Felicidad',
          population: graficaCantidades[0].felicidad,
          color: '#FFCE5F',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Ira',
          population: graficaCantidades[0].ira,
          color: '#F87374',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Tristeza',
          population: graficaCantidades[0].tristeza,
          color: '#5C83FE',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Miedo',
          population: graficaCantidades[0].miedo,
          color: '#AA5CFE',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Disgusto',
          population: graficaCantidades[0].disgusto,
          color: '#A1DD56',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Sorpresa',
          population: graficaCantidades[0].sorpresa,
          color: '#F6954E',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
      ];

      let grafica2Model = [
        {
          name: 'Felicidad',
          population: graficaCantidades[1].felicidad,
          color: '#FFCE5F',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Ira',
          population: graficaCantidades[1].ira,
          color: '#F87374',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Tristeza',
          population: graficaCantidades[1].tristeza,
          color: '#5C83FE',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Miedo',
          population: graficaCantidades[1].miedo,
          color: '#AA5CFE',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Disgusto',
          population: graficaCantidades[1].disgusto,
          color: '#A1DD56',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Sorpresa',
          population: graficaCantidades[1].sorpresa,
          color: '#F6954E',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
      ];

      let grafica3Model = [
        {
          name: 'Felicidad',
          population: graficaCantidades[2].felicidad,
          color: '#FFCE5F',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Ira',
          population: graficaCantidades[2].ira,
          color: '#F87374',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Tristeza',
          population: graficaCantidades[2].tristeza,
          color: '#5C83FE',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Miedo',
          population: graficaCantidades[2].miedo,
          color: '#AA5CFE',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Disgusto',
          population: graficaCantidades[2].disgusto,
          color: '#A1DD56',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Sorpresa',
          population: graficaCantidades[2].sorpresa,
          color: '#F6954E',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
      ];

      let grafica4Model = [
        {
          name: 'Felicidad',
          population: graficaCantidades[3].felicidad,
          color: '#FFCE5F',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Ira',
          population: graficaCantidades[3].ira,
          color: '#F87374',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Tristeza',
          population: graficaCantidades[3].tristeza,
          color: '#5C83FE',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Miedo',
          population: graficaCantidades[3].miedo,
          color: '#AA5CFE',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Disgusto',
          population: graficaCantidades[3].disgusto,
          color: '#A1DD56',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Sorpresa',
          population: graficaCantidades[3].sorpresa,
          color: '#F6954E',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
      ];

      let grafica5Model = [
        {
          name: 'Felicidad',
          population: graficaCantidades[4].felicidad,
          color: '#FFCE5F',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Ira',
          population: graficaCantidades[4].ira,
          color: '#F87374',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Tristeza',
          population: graficaCantidades[4].tristeza,
          color: '#5C83FE',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Miedo',
          population: graficaCantidades[4].miedo,
          color: '#AA5CFE',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Disgusto',
          population: graficaCantidades[4].disgusto,
          color: '#A1DD56',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Sorpresa',
          population: graficaCantidades[4].sorpresa,
          color: '#F6954E',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
      ];

      let grafica6Model = [
        {
          name: 'Felicidad',
          population: graficaCantidades[5].felicidad,
          color: '#FFCE5F',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Ira',
          population: graficaCantidades[5].ira,
          color: '#F87374',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Tristeza',
          population: graficaCantidades[5].tristeza,
          color: '#5C83FE',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Miedo',
          population: graficaCantidades[5].miedo,
          color: '#AA5CFE',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Disgusto',
          population: graficaCantidades[5].disgusto,
          color: '#A1DD56',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
        {
          name: 'Sorpresa',
          population: graficaCantidades[5].sorpresa,
          color: '#F6954E',
          legendFontColor: '#000',
          legendFontSize: 12,
        },
      ];

      //Añadimos el modelo a la gráfica 1
      setGrafica(grafica1Model);

      setIsLoading(false);
    });
  };

  let chartConfig = {
    backgroundColor: '#e26a00',
    backgroundGradientFrom: '#fb8c00',
    backgroundGradientTo: '#ffa726',
    decimalPlaces: 2, // optional, defaults to 2dp
    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    style: {
      borderRadius: 16,
    },
    propsForDots: {
      r: '6',
      strokeWidth: '2',
      stroke: '#ffa726',
    },
  };

  if (isLoading) {
    return (
      <Center>
        <Spinner color='pink.500' size={'lg'} />
      </Center>
    );
  }

  return (
    <Box>
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={() => setIsLoading(true)}
          />
        }
      >
        <Box margin={2}>
          <Heading fontSize='xl' p='4' pb='3'>
            Emociones
          </Heading>
        </Box>
        <PieChart
          data={grafica}
          width={ScreenWidth}
          height={200}
          chartConfig={chartConfig}
          accessor={'population'}
          backgroundColor={'transparent'}
          paddingLeft={'0'}
          center={[ScreenWidth / 15, 0]}
          hasLegend={true}
          absolute
        />
      </ScrollView>
    </Box>
  );
}
export { Tablero };
