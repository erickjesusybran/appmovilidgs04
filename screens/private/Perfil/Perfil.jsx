import { createUserWithEmailAndPassword, getAuth } from 'firebase/auth';
import { addDoc, collection, doc, getDocs, getFirestore, onSnapshot } from 'firebase/firestore';
import { getDownloadURL, getStorage, ref } from 'firebase/storage';
import {
	Avatar, Box, Button, Center, Circle, Heading, HStack, NativeBaseProvider,
	ScrollView, Spinner, Square, Text, useToast, VStack
} from 'native-base';
import React, { useContext, useEffect, useState } from 'react';
import { RefreshControl } from 'react-native';
import { app } from '../../../firebase/config';
import { UsuarioDocContext } from '../../../hooks/useUsuarioDoc';

function Perfil({ navigation }) {
	//Usar a conveniencia (instanciando esto :D) en cualquier vista
	const usuarioContext = useContext(UsuarioDocContext);

	// instancias de firebase/nativebase
	const db = getFirestore(app);
	const auth = getAuth(app);
	const storage = getStorage(app);
	const toast = useToast();

	const [usuarioDoc, setUsuarioDoc] = useState(null);
	const [fotoPerfilHttpUrl, setFotoPerfilHttpUrl] = useState('')
	const [isLoading, setIsLoading] = useState(true);
	const [refreshing, setRefreshing] = useState(false);
	//Refs

	useEffect(() => {
		getUsuarioDoc();
	}, [isLoading])

	const getUsuarioDoc = () => {
		onSnapshot(doc(db, 'usuarios', usuarioContext.id), doc => {
			setUsuarioDoc({ ...doc.data(), id: doc.id })
			setIsLoading(false);
			console.log(doc.data())

			//Obtiene la foto de perfil
			if (doc.data().fotoPerfil.length > 0) {
				getDownloadURL(ref(storage, doc.data().fotoPerfil)).then(url => {
					setFotoPerfilHttpUrl(url);
				})
			}
		});
	}

	if (isLoading) {
		return (
			<Box flex={1} bg={'gray.50'}>
				<Center>
					<Spinner size={'lg'} color={'rose.500'} />
				</Center>
			</Box>
		)
	}

	return (
		<Box flex={1} bg={'gray.50'}>
			<ScrollView refreshControl={<RefreshControl
				refreshing={isLoading}
				onRefresh={() => setIsLoading(true)}
			/>}>
				<VStack space={4} px={6}>
					<Center>
						<Avatar
							bg="rose.500"
							marginTop={15}
							size="2xl"
							source={{ uri: fotoPerfilHttpUrl }}
							mb={2}>
							<Heading color={'white'} size={'3xl'}>
								{usuarioDoc.email.charAt(0)}
							</Heading>
						</Avatar>
						<Heading mb={4}>
							{usuarioDoc.nombres} {usuarioDoc.apellidos}
						</Heading>
						<Button colorScheme={'rose'} onPress={() =>
							navigation.navigate('EditarPerfil', { usuarioData: usuarioDoc })}>
							EDITAR PERFIL
						</Button>
					</Center>

					<Box bg={'white'} p={4} my={3} borderRadius={'lg'} shadow={'3'}>
						<Heading mb={4}>
							Mis datos
						</Heading>
						<Box>
							<Text bold>
								Correo electrónico
							</Text>
							<Text fontSize="sm" >
								{usuarioDoc.email}
							</Text>
						</Box>
						<Box>
							<Text bold>
								Escuela
							</Text>
							<Text fontSize="sm" >
								{usuarioDoc.escuela}
							</Text>
						</Box>
						<Box>
							<Text bold>
								Edad
							</Text>
							<Text fontSize="sm" >
								{usuarioDoc.edad}
							</Text>
						</Box>
					</Box>
				</VStack>
			</ScrollView>
		</Box>
	)
}

export {
	Perfil
};
