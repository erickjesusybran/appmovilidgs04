import * as ImagePicker from 'expo-image-picker';
import { getStorage, ref, uploadBytes } from 'firebase/storage';
import { collection, doc, getDocs, getFirestore, updateDoc } from 'firebase/firestore';
import {
    Avatar, Box, Button, FormControl, HStack, Input, ScrollView, Select,
    Text, useToast, VStack
} from 'native-base';
import React, { useEffect, useState } from 'react';
import { app } from '../../../firebase/config';
import { focusStyles } from '../../../utils/styles/inputs';
import { minLengthValidation } from '../../../utils/validation/Form';
import { FontAwesome } from '@expo/vector-icons';

/**
 * 
 * @param {*} {navigation} 
 * @returns React.Node
 */
function EditarPerfil({ route, navigation }) {
    //Props
    const { usuarioData } = route.params;

    // instancias de firebase/nativebase
    const db = getFirestore(app);
    const storage = getStorage(app);

    //Refs
    const escuelasRef = collection(db, 'escuelas');

    const [usuario, setUsuario] = useState(usuarioData);
    const [escuelas, setEscuelas] = useState([]);
    const [archivo, setArchivo] = useState(null);
    const [imagenUri, setImagenUri] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const toast = useToast();

    useEffect(() => {
        getEscuelasDocs();

        return () => {
            setEscuelas([]);
        }
    }, []);

    /**
     * Función de traer escuelas
     * @returns void 
     */
    const getEscuelasDocs = () => {
        getDocs(escuelasRef).then(QuerySnapshot => {
            let escuelasStored = [];

            QuerySnapshot.forEach(doc => {
                escuelasStored.push(doc.data());
            })

            setEscuelas(escuelasStored);
        });
    }

    //funcion de seleccion de imagen por galeria
    const pickImage = async () => {
        //preguntar al usuario por permiso para acceso a la libreria de galeria
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();

        if (status === 'granted') {
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                aspect: [4, 4],
                quality: 1,
            });

            //este if subira la imagen en firestore con un nombre = a id de usuario
            if (!result.cancelled) {
                setImagenUri(result.uri);

                const blobImagen = await (await fetch(result.uri)).blob();

                setArchivo(new File(
                    [blobImagen],
                    `img-${usuario.id}`,
                    { type: 'image/jpeg' }
                ))
            } else {
                toast.show({
                    description: "Selecciona una foto."
                });
            }
        } else {
            toast.show({ description: "Necesitamos permisos para acceder a tu galería." });
        }
    };


    /**
     * Funcion para uso de camara
     * 
     * @returns void
     */
    const openCamera = async () => {
        // Ask the user for the permission to access the camera
        const { status } = await ImagePicker.requestCameraPermissionsAsync();

        if (status === 'granted') {
            const result = await ImagePicker.launchCameraAsync();

            //este if subira la imagen en firestore con un nombre = a id de usuario
            if (!result.cancelled) {
                setImagenUri(result.uri);

                const blobImagen = await (await fetch(result.uri)).blob();

                setArchivo(new File(
                    [blobImagen],
                    `img-${usuario.id}`,
                    { type: 'image/jpeg' }
                ))
            } else {
                toast.show({
                    description: "Toma una foto."
                });
            }
        } else {
            toast.show({ description: "Necesitamos permisos para acceder a tu cámara." });
        }
    }

    /**
     * Función para subir la imagen a firebase/storage
     * 
     * @returns string
     */
    const uploadImage = async () => {
        const storageRef = ref(storage, `fotos-perfil/${usuario.id}`);

        return await (await uploadBytes(storageRef, archivo)).ref.toString();
    };

    /**
     * Función para modificar usario
     * @return void
     */

    const updateUsuarioDoc = async () => {
        const newUsuario = {
            nombres: usuario.nombres,
            apellidos: usuario.apellidos,
            escuela: usuario.escuela,
            edad: usuario.edad,
            fotoPerfil: usuario.fotoPerfil
        };

        if (!minLengthValidation(newUsuario.escuela, 1)) {
            toast.show({ description: "Seleccione una escuela" });
            return;
        }

        setIsLoading(true);

        //Carga la imagen si selecciono una
        if (archivo) {
            newUsuario.fotoPerfil = await uploadImage();
        }

        updateDoc(doc(db, 'usuarios', usuario.id), newUsuario).then(() => {
            setIsLoading(false);
            toast.show({ description: 'Datos de perfil modificado' })
            navigation.goBack();
        }).catch(e => {
            setIsLoading(false);
            toast.show(e.message);
        })
    }

    return (
        <Box flex={1} bg={'white'}>
            <ScrollView>
                <VStack space={4} px={6} my={4}>
                    <Avatar
                        bg="pink.600"
                        alignSelf="center"
                        marginTop={15}
                        size="2xl"
                        source={{ uri: imagenUri }}>
                        <FontAwesome name="camera" size={24} color="white" />
                        <Text textAlign={'center'} color={'white'} mt={2}>
                            Cambiar foto de perfil
                        </Text>
                    </Avatar>
                    <Box alignItems="center" marginTop={15}>
                        <HStack flex={1} >
                            <Button marginRight={2} onPress={openCamera} colorScheme={'rose'}>
                                ABRIR CÁMARA
                            </Button>
                            <Button marginLeft={2} onPress={pickImage} colorScheme={'rose'}>
                                SELECCIONAR IMAGEN
                            </Button>
                        </HStack>
                    </Box>

                    <VStack space={4} my={4}>
                        <Text color={'darkBlue.800'}>* Ningún debe estar vacío</Text>
                        <FormControl>
                            <FormControl.Label>Nombres</FormControl.Label>
                            <Input
                                value={usuario.nombres}
                                onChange={e => setUsuario({ ...usuario, nombres: e.nativeEvent.text })}
                                placeholder="Nombres"
                                _focus={focusStyles} />
                            <FormControl.ErrorMessage>
                                Ingrese un nombre valido
                            </FormControl.ErrorMessage>
                        </FormControl>
                        <FormControl>
                            <FormControl.Label>Apellidos</FormControl.Label>
                            <Input
                                value={usuario.apellidos}
                                onChange={e => setUsuario({ ...usuario, apellidos: e.nativeEvent.text })}
                                placeholder="Nombres"
                                _focus={focusStyles} />
                            <FormControl.ErrorMessage>
                                Ingrese un nombre valido
                            </FormControl.ErrorMessage>
                        </FormControl>

                        <FormControl>
                            <FormControl.Label>Escuela</FormControl.Label>
                            <Select
                                defaultValue={usuario.escuela}
                                onValueChange={i => setUsuario({ ...usuario, escuela: i })}>
                                {
                                    escuelas.map((value, index) => (
                                        <Select.Item label={value.nombre} value={value.nombre} key={index} />
                                    ))
                                }
                            </Select>
                        </FormControl>
                        <FormControl>
                            <FormControl.Label>Edad</FormControl.Label>
                            <Input
                                onChange={e => setUsuario({ ...usuario, edad: e.nativeEvent.text })}
                                value={usuario.edad}
                                placeholder="Edad"
                                keyboardType="number-pad"
                                _focus={focusStyles} />
                        </FormControl>
                    </VStack>
                    <Button
                        isLoading={isLoading}
                        onPress={updateUsuarioDoc}
                        marginTop={8}
                        colorScheme={'pink'}>
                        Guardar cambios
                    </Button>
                </VStack>
            </ScrollView>
        </Box>

    )
}

export {
    EditarPerfil
};
