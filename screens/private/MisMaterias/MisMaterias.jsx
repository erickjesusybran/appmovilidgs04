import { Button, HStack, NativeBaseProvider, Text, ScrollView, List, Box } from 'native-base';
import React, { useContext, useState, useEffect } from 'react';
import { StyleSheet } from 'react-native';
import { MaterialIcons, FontAwesome } from '@expo/vector-icons';
import { Item } from './Item';
import { UsuarioDocContext } from '../../../hooks/useUsuarioDoc'
//firestore
import { app } from '../../../firebase/config'
import {
  getFirestore, collection, getDoc, where, query, doc,
  onSnapshot
} from 'firebase/firestore'

function MisMaterias({ navigation, props }) {

  //Usar a conveniencia (instanciando esto :D) en cualquier vista
  const usuario = useContext(UsuarioDocContext);
  //borrar esta línea
  console.log(usuario.id);

  const [usuarioUpdate, setUsuarioUpdate] = useState(null);

  const db = getFirestore(app);

  //Efecto que trae los datos del usuario para sus materias
  useEffect(() => {

    usuarioDatos();

    return () => {
      setUsuarioUpdate(null)
    }
  }, [])


  /**
 * Función para traer las escuelas
 * @returns void
 */
  const usuarioDatos = () => {
    const docRef = doc(db, "usuarios", usuario.id);
    onSnapshot(docRef, doc => {
      setUsuarioUpdate(doc.data())
    })
  };


  /*
    useEffect(() => {
      const subscriber = firestore()
        .collection("usuarios")
        .doc(usuario.id)
        .onSnapshot(documentSnapshot => {
          setUsuarioUpdate(documentSnapshot.data())
          console.log('User data: ', documentSnapshot.data());
        });
  
      // Stop listening for updates when no longer required
      return () => subscriber();
    }, []);
    */

  if (!usuarioUpdate) {
    return <Text>Cargando</Text>
  }

  return (
    <NativeBaseProvider>
      <ScrollView>
        <List>
          {
            usuarioUpdate.materias.map((materia, index) => <Item nombre={materia.nombre} key={index} />)
          }
        </List>
      </ScrollView>

      <Button colorScheme={'pink'} style={styles.boton} marginTop={8} shadow={2} onPress={() =>
        navigation.navigate('RegistrarMateria', {
          usuario: usuario
        })
      }>
        <HStack>
          <FontAwesome
            name='plus-circle'
            size={24}
            color={'#fff'}
          />
          <Text color={'#fff'} >  Registrar materia</Text>
        </HStack>

      </Button>
    </NativeBaseProvider>
  )
}

//estilos para el boton de agregar materia
const styles = StyleSheet.create({
  boton: {
    position: 'absolute',
    top: '85%',
    left: '50%',
    color: '#fff',

  },
  red: {
    color: 'red',
  },
});


export {
  MisMaterias
}