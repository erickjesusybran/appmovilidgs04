import { Box, HStack, Text } from 'native-base';
import React from 'react';
import { MaterialIcons, FontAwesome } from '@expo/vector-icons';

function Item(props) {
  const { nombre } = props;
  /**
   *             <FontAwesome
                name='plus-circle'
                size={24}
                color={'#fff'}
              />
              <FontAwesome
                name='plus-circle'
                size={24}
                color={'#fff'}
              />
   */

  return (
    <Box padding={'10px'}>
        <HStack>
            <Text>{ nombre }</Text>
        </HStack>
    </Box>
  )
}

export { Item }