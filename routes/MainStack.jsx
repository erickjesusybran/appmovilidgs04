import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import React from 'react'
import { UsuarioDocContext } from '../hooks/useUsuarioDoc';
//Views
import Registro from "../screens/public/Registro/Registro";
import InicioSesion from "../screens/public/InicioSesion/InicioSesion";
import { MainTab } from './MainTab';
import { RegistrarMateria } from '../screens/private/RegistrarMateria/RegistrarMateria';
import { EditarPerfil } from '../screens/private/EditarPerfil/EditarPerfil';
import { options } from '../utils/styles/headers';

const Stack = createNativeStackNavigator();

const MainStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="InicioSesion" component={InicioSesion} options={{
          headerShown: false
        }} />
        <Stack.Screen name="Registro" component={Registro} options={options} />
        <Stack.Screen name="MainTab" component={MainTab} options={{ ...options, headerShown: false }} />
        <Stack.Screen name="RegistrarMateria" component={RegistrarMateria} options={options} />
        <Stack.Screen name="EditarPerfil" component={EditarPerfil} options={options} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default MainStack