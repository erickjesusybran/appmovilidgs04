import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { theme } from 'native-base';
import React from 'react';
import { UsuarioDocContext } from '../hooks/useUsuarioDoc';
import { Perfil } from '../screens/private/Perfil/Perfil';
import { MisMaterias } from '../screens/private/MisMaterias/MisMaterias';
import { Graficas } from '../screens/private/Graficas/Graficas';
import { Tablero } from '../screens/private/Tablero/Tablero';
import { MaterialIcons, FontAwesome } from '@expo/vector-icons';



const Tab = createBottomTabNavigator();

function MainTab({ route, navigation, props, focused }) {
  return (
    <UsuarioDocContext.Provider value={route.params.user}>
      <Tab.Navigator>
        <Tab.Screen
          name='Tablero'
          component={Tablero}
          options={{
            title: 'Tablero',
            headerStyle: {
              backgroundColor: '#DC202B',
            },
            headerTintColor: '#FFFFFF',
            tabBarIcon: (props) => (
              <MaterialIcons
                name='dashboard'
                size={24}
                color={
                  props.focused
                    ? theme.colors.rose[500]
                    : theme.colors.gray[500]
                }
              />
            ),
            tabBarActiveTintColor: theme.colors.rose[500],
            tabBarInactiveTintColor: theme.colors.gray[500],
          }}
        />
        <Tab.Screen
          name='Mis materias'
          component={MisMaterias}
          options={{
            title: 'Mis materias',
            headerStyle: {
              backgroundColor: '#DC202B',
            },
            headerTintColor: '#FFFFFF',
            tabBarIcon: (props) => (
              <FontAwesome
                name='book'
                size={24}
                color={
                  props.focused
                    ? theme.colors.rose[500]
                    : theme.colors.gray[500]
                }
              />
            ),
            tabBarActiveTintColor: theme.colors.rose[500],
            tabBarInactiveTintColor: theme.colors.gray[500],
          }}
        />
        <Tab.Screen
          name='Graficas'
          component={Graficas}
          options={{
            title: 'Graficas',
            headerStyle: {
              backgroundColor: '#DC202B',
            },
            headerTintColor: '#FFFFFF',
            tabBarIcon: (props) => (
              <FontAwesome
                name='pie-chart'
                size={24}
                color={
                  props.focused ? theme.colors.rose[500] : theme.colors.gray[500]
                }
              />
            ),
            tabBarActiveTintColor: theme.colors.rose[500],
            tabBarInactiveTintColor: theme.colors.gray[500],
          }}
        />
        <Tab.Screen
          name='Perfil'
          component={Perfil}
          options={{
            title: 'Perfil',
            headerStyle: {
              backgroundColor: '#DC202B',
            },
            headerTintColor: '#FFFFFF',
            tabBarIcon: (props) => (
              <FontAwesome
                name='user'
                size={24}
                color={
                  props.focused
                    ? theme.colors.rose[500]
                    : theme.colors.gray[500]
                }
              />
            ),
            tabBarActiveTintColor: theme.colors.rose[500],
            tabBarInactiveTintColor: theme.colors.gray[500],
          }}
        />
      </Tab.Navigator>
    </UsuarioDocContext.Provider>
  );
}

export { MainTab };
